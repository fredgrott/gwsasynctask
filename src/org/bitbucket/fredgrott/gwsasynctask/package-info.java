/**
 * 
 */
/**
 * Set of classes to enable specifying execution of asynctask 
 * either as parallel or serial and some other added features.
 * 
 * @author fredgrott
 *
 */
package org.bitbucket.fredgrott.gwsasynctask;